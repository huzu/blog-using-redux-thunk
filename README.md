## Important Points:

1. In our action creator, we have marked the 1st approach as bad. The reason is that it returns an error message as "Action creators must return plain JS object. Use custom middleware for async functions."
2. This is because the ES2015 translated version of the code that we have written as `async` and `await` methods gets transformed to switch case where the request's response is returned and not the type hash that we have returned. 
3. Even if we remove the `async` and `await` keywords, the issue doesn't gets solved though the error message will no longer be visible. 
4. It won't work as the action creators are dispatched to react redux library and are then given to reducers where it's state gets updated. 
5. Now this whole process gets completed in a fraction of milliseconds, hence placing an api call inside the action creator won't work as the api will take some amount of time to return the response but the control would have been already transfered further. 
6. This is where the react middleware comes into action. Here instead of the action getting transfered to reducers directly from dispatch, they are first forwarded to middleware where they are getting executed. 
7. Normal rules of action creators are: i) Action creators must return action objects. ii) actions must have a type property. iii) actions can optionally have a payload. 
8. Rules for redux thunk are: i) Action creators can return action objects or can return functions. ii) If an object gets returned, it must have a type property. iii) actions can optionally have a payload
9. If an object is passed to redux thunk middleware, it simply passes to reducers as it doesn't need to deal with it at all. 
10. But if a function is passed to the middleware then dispatch and getState methods are called on it. It waits for it to get completed and then a new action is created. This new action is again fed to the middleware and now if its still a function this process gets repeated until we get a plain JS object. 
11. Some rules of reducers are: 
    a. A reducer must have a return value which can be anything, it can be a number, string, array etc. If there is no return value it will return undefined which will throw an error in our react application. Hence, a reducer can never ever return undefined.
    b. It should not mutate its state argument. Mutation in javascript refers to change in the contents of an object which can be addition, deletion or updation. Note that in javascript a string or a number is immutable which means their value can't be changed. 

12. To add an element in array, use
    `arr = ['red', 'blue'];
     [...arr, 'green']
    `
    This will return a brand new array without changing the original arr. 
13. We have also used memoize function lodash library to save application from multiple api calls.
