import _ from 'lodash';
import jsonPlaceholder from '../apis/jsonPlaceholder';


export const fetchPostsAndUsers = () => async (dispatch, getState) => {  
  await dispatch(fetchPosts());

  const userIds =  _.uniq(_.map(getState().posts, 'userId'));
  userIds.forEach(id => dispatch(fetchUser(id)));
}

export const fetchPosts = () => async dispatch => {
  const response = await jsonPlaceholder.get('/posts');
  
  dispatch({ type: 'FETCH_POSTS', payload: response.data })
}; 

export const fetchUser = (id) => async dispatch => {
  const response = await jsonPlaceholder.get(`/users/${id}`);
  dispatch({ type: 'FETCH_USER', payload: response.data});
}

// here memoize caches the result of the action once fired but the downside is if the object is changed or updated, there is no way to fire a new request, hence we would require to write the same action creator without memoize function.
// hence we will not be using this approach. 
// const _fetchUser = _.memoize(async (id, dispatch) => {
//   const response = await jsonPlaceholder.get(`/users/${id}`);
//   dispatch({ type: 'FETCH_USER', payload: response.data})
// });
